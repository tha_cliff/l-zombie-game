#include <iostream>
#include "game.h"

using namespace std;

#undef main

int main()
{
	cout << "<main> Started!" << endl;
	Game game;
	game.Init();
	if(game.Run() == false)
	{
		cout << "<main> Game.run returned False!" << endl;
	}
	
	return 0;
}