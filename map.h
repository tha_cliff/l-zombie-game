#ifndef __MAP
#define __MAP

#include "SDL.h"
#include "SDL_image.h"
#include <iostream>

using namespace std;

class Map
{
	private:
		
	public:
		Map();
		~Map();
		bool Update();
		bool Render();
};

#endif