#include "game.h"

Game::Game()
{
	cout << "<game> Game Constructor!" << endl;
	screenDimension.w = 800;
	screenDimension.h = 600;
	running = true;
}

Game::~Game()
{
	cout << "<game> Game Destroyed!" << endl;
	SDL_Quit();
}

bool Game::Init()
{
	SDL_Init( SDL_INIT_EVERYTHING );	 // Initializes SDL

	display = SDL_SetVideoMode( screenDimension.w, 
								screenDimension.h, 
								32, 
								SDL_HWSURFACE | SDL_DOUBLEBUF 
							  );						// Initializes a window, and saves the Adress to 'display'

	return true;
}

bool Game::Run()
{
	while(running)
	{
		ProcessEvents();
		Update();
		Render();
	}
	return true;
}

void Game::Update() // This function 
{
	map.Update();
}

void Game::Render()
{
	map.Render();
}

void Game::ProcessEvents()
{
	SDL_Event event; 					// Creates a placeholder for the eventHandler
	while ( SDL_PollEvent( &event ) ) 	// Begins to go trough Events. Polls 1 event
	{
		if ( event.type == SDL_QUIT ) 	// If event is "Kill the goddamn game"
		{
			cout << "Game: SDL_QUIT event occured!" << endl;
			running = false; 			// Quit the game in the end of this cycle
		}
	}
}
