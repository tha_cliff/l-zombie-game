#ifndef __GAME
#define __GAME

#include "map.h"
#include "SDL.h"
#include "SDL_image.h"
#include <iostream>

using namespace std;

class Game
{
	private:
		bool running;
		SDL_Surface* display;
		SDL_Rect screenDimension;
		void ProcessEvents();
		void Render();
		void Update();
		
		Map map;
	public:
		Game();
		~Game();
		bool Run();
		bool Init();
};

#endif